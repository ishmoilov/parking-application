﻿using System;
using System.Net.Mime;
using System.Threading;

using ParkingApp.Classes;

namespace ParkingApp
{
    internal class Program
    {

        public static void Main(string[] args)
        {
            while (true)
            {
                PrintStartingMenu();
                int.TryParse(Console.ReadLine(),out var userInput);
                switch (userInput)
                {
                    case 1:
                        Console.Clear();
                        PrintCurrentParkingBalance();
                        break;
                    case 2:
                        Console.Clear();
                        PrintLatestEarnedAmount();
                        break;
                    case 3:
                        Console.Clear();
                        PrintOccupancyStatus();
                        break;
                    case 4:
                        Console.Clear();
                        PrintParkingLastMinuteTransactions();
                        break;
                    case 5:
                        Console.Clear();
                        PrintAllParkingTransactions();
                        break;
                    case 6:
                        Console.Clear();
                        PrintParkedVehiclesList();
                        break;
                    case 7:
                        Console.Clear();
                        AddVehicleToParking();
                        break;
                    case 8:
                        Console.Clear();
                        RemoveVehicleFromParking();
                        break;
                    case 9:
                        Console.Clear();
                        ReplenishVehicleBalance();
                        break;
                    case 0:
                        Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("Your input was invalid");
                        break;
                }
                Console.WriteLine("Press any key to return to menu");
                Console.ReadKey();
            }
        }


        private static void PrintLatestEarnedAmount()
        {
            Console.WriteLine($"Parking have earned {ParkingAccountManager.GetLatestEarnedAmount()} in last minute");
        }
        private static void PrintCurrentParkingBalance()
        {
            Console.WriteLine($"Current parking balance is {ParkingAccountManager.ParkingBalance}");
        }
        
        private static void PrintOccupancyStatus()
        {
            Console.WriteLine(Parking.GetOccupancyStatus());
        }

        private static void PrintParkedVehiclesList()
        {
            Parking.ParkedVehicles.ForEach(vehicle =>
            {
                Console.WriteLine($"{vehicle.Make} {vehicle.Color}, Plate Number : {vehicle.LicenseNumber}");
            });
        }
        
        private static void PrintAllParkingTransactions()
        {
            Console.WriteLine(ParkingAccountManager.GetAllTransactions());
        }


        private static void PrintParkingLastMinuteTransactions()
        {
            var transactions = ParkingAccountManager.ParkingTransactions;
            transactions.ForEach(transaction =>
            {
                Console.WriteLine($"{transaction.TransactionTime} {transaction.VehicleLicenceNumber} {transaction.ChargedAmount}");
            });
        }
        private static void ReplenishVehicleBalance()
        {
            Console.WriteLine("Enter plate number of vehicle you want to replenish balance for");
            var plateNumber = Console.ReadLine();
            
            Console.Write("Enter amount you want to add to balance");
            var amount = Convert.ToDecimal(Console.ReadLine());
            var foundVehicle = Parking.ParkedVehicles.Find(vehicle => vehicle.LicenseNumber == plateNumber);

            if (foundVehicle != null)
            {
                foundVehicle.AccountManager.AddFunds(amount);
                Console.WriteLine($"Vehicle's balance was replenished for {amount} ");
            }
            else
            {
                Console.WriteLine("There is no vehicle with such plate number on the parking");
            }
        }


        private static void AddVehicleToParking()
        {
            Console.WriteLine("What type of vehicle do you have?");
            Console.WriteLine("1 - Car, 2 - Truck, 3 - Bus, 4 - Motorcycle");
            int.TryParse(Console.ReadLine(),out var vehicleType);
            Vehicle newVehicle = null;
            switch (vehicleType)
            {
                case 1:
                    newVehicle = new Car(string.Empty,string.Empty,string.Empty);
                    break;
                case 2:
                    newVehicle = new Truck(string.Empty,string.Empty,string.Empty);
                    break;
                case 3:
                    newVehicle = new Bus(string.Empty,string.Empty,string.Empty);
                    break;
                case 4:
                    newVehicle = new Motorcycle(string.Empty,string.Empty,string.Empty);
                    break;
                default:
                    Console.WriteLine("No such vehicle type, try again");
                    AddVehicleToParking();
                    break;
            }

            if (newVehicle == null) return;
            Console.WriteLine("Please, enter your vehicle Data");
            Console.Write("Enter your license plate number: ");
            newVehicle.LicenseNumber = Console.ReadLine();
            Console.Write("Enter your car make or press enter to skip: ");
            newVehicle.Make = Console.ReadLine();
            Console.Write("Enter your car color or press enter to skip: ");
            newVehicle.Color = Console.ReadLine();

            var returnedTuple = newVehicle.Park(ParkingSettings.ParkingCapacity,Parking.ParkedVehicles);
            Console.WriteLine(returnedTuple?.Item2);

        }

        private static void RemoveVehicleFromParking()
        {
            Console.WriteLine("Enter plate number of vehicle you want to remove");
            var number = Console.ReadLine();
            var unparkingResult = Parking.ParkedVehicles.Find(vehicle => vehicle.LicenseNumber == number)
                                                        .Unpark(Parking.ParkedVehicles);
            
            Console.WriteLine(unparkingResult.Item2);
        }

        private static void PrintStartingMenu()
        {
            Console.WriteLine("Welcome to Parking Application. Select your option");
            Console.WriteLine("1 - Get current parking balance.");
            Console.WriteLine("2 - Get sum of funds earned in last minute"); 
            Console.WriteLine("3 - Get parking spaces state");
            Console.WriteLine("4 - Print all parking transactions from last minute");
            Console.WriteLine("5 - Print all parking transactions history");
            Console.WriteLine("6 - Print parked vehicles list");
            Console.WriteLine("7 - Add vehicle to the parking lot");
            Console.WriteLine("8 - Remove vehicle from the parking lot");
            Console.WriteLine("9 - Replenish vehicle's balance");
            Console.WriteLine("0 - Exit application");
        }
    }
}