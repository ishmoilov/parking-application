namespace ParkingApp.Classes
{
    public class Motorcycle: Vehicle
    {
        public Motorcycle(string licenseNumber, string make = "Make not provided", string color = "Color not provided") : base(licenseNumber, make, color)
        {
        }
    }
}