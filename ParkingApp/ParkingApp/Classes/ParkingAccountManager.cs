using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParkingApp.Classes
{
    public static class ParkingAccountManager
    {
        private const int DefaultBalance = 0;
        public static List<Transaction> ParkingTransactions { get; }
        
        public static decimal ParkingBalance { get; set; }

        static ParkingAccountManager()
        {
            ParkingTransactions = new List<Transaction>();
            ParkingBalance = DefaultBalance;
        }

        public static void ChargeParkedVehicles(List<Vehicle> parkedVehicles)
        {
            parkedVehicles.ForEach(vehicle =>
            {
                var calculatedRate = ParkingSettings.CalculateRateByVehicleType(vehicle);
                
                var chargedAmount =  vehicle.AccountManager.Balance >= calculatedRate
                                    ? calculatedRate
                                    : calculatedRate * ParkingSettings.PenaltyRate;

               vehicle.AccountManager.WithdrawFunds(chargedAmount);
               ParkingBalance += chargedAmount;
                
                ParkingTransactions.Add(new Transaction
                {
                    ChargedAmount = chargedAmount,
                    TransactionTime = DateTime.Now,
                    VehicleLicenceNumber = vehicle.LicenseNumber
                });
                
            });
        }

        public static decimal GetLatestEarnedAmount()
        {
            decimal earnedAmount = 0;
            ParkingTransactions.ForEach(transaction => { earnedAmount += transaction.ChargedAmount; });
            return earnedAmount;
        }

        public static StringBuilder GetAllTransactions()
        {
            return TransactionLogger.GetTransactionsHistory();
        }
    }
}