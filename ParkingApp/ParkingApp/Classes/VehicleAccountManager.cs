using System;

namespace ParkingApp.Classes
{
    public  class VehicleAccountManager
    {
        private const decimal DefaultBalance = 50;
        public decimal Balance { get; set; }

        public VehicleAccountManager()
        {
            Balance = DefaultBalance;
        }

        public VehicleAccountManager(decimal startingBalance)
        {
            Balance = startingBalance;
        }

        public void AddFunds(decimal amount)
        {
            Balance += amount;
        }

        public void WithdrawFunds(decimal amount)
        {
            Balance -= amount;
        }
    }
}