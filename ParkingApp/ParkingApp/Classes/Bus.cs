namespace ParkingApp.Classes
{
    public class Bus: Vehicle
    {
        public Bus(string licenseNumber, string make = "Make not provided", string color = "Color not provided") : base(licenseNumber, make, color)
        {
        }
    }
}