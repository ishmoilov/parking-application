using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Threading;

namespace ParkingApp.Classes
{
    public static class TransactionLogger
    {
        private const int LoggingTimerInMinutes = 1;
        private const string Path = @"G:\Binary Studio\parking-application\ParkingApp\logs\";
        
        private const string  FileName = "parking transactions.log";
        private static Timer _timer = new Timer(e => LogTransactionsScope(ParkingAccountManager.ParkingTransactions) ,
            null,
            TimeSpan.Zero,
            TimeSpan.FromMinutes(LoggingTimerInMinutes));

        static TransactionLogger()
        {
        }
        public static StringBuilder GetTransactionsHistory()
        {
            var outputString = new StringBuilder();
            if (File.Exists(Path + FileName))
            {
                var textTransactions = File.ReadAllLines(Path + FileName);
                foreach (var transaction in textTransactions)
                {
                    outputString.AppendLine(transaction);
                }    
            }
            else
            {
                outputString.Append("No transactions were stored in logs yet");
            }

            return outputString;
        }

        private static void LogTransactionsScope(List<Transaction> transactionsScope)
        {
            transactionsScope.ForEach(LogTransaction);
            transactionsScope.Clear();
        }

        private static void LogTransaction(Transaction transaction)
        {
            VerifyDirectory(Path);
            try
            {
                using (var file = new StreamWriter(Path + FileName, true))
                {
                    file.WriteLine(
                        $"Date: {transaction.TransactionTime.ToString(CultureInfo.InvariantCulture)}, " +
                               $"Vehicle ID: {transaction.VehicleLicenceNumber}, " +
                               $"Charged Amount: {transaction.ChargedAmount}");
                    file.Close();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void VerifyDirectory(string path)
        {
            try
            {
                var directory = new DirectoryInfo(path);
                if (!directory.Exists)
                {
                    directory.Create();
                }
            }
            catch
            {
                var exception = new Exception();
                Console.WriteLine(exception.Message);
            }
        }
    }
}