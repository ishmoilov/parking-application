namespace ParkingApp.Classes
{
    public class Car: Vehicle
    {
        public Car(string licenseNumber, string make = "Make not provided", string color = "Color not provided") : base(licenseNumber, make, color)
        {
            
        }
    }
}