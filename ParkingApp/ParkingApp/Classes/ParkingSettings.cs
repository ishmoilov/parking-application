
namespace ParkingApp.Classes
{
    public static class ParkingSettings
    {
        
        private const int DefaultCapacity = 10;
        private const int DefaultChargingPeriod = 5;
        private const decimal DefaultCarRate = 2;
        private const decimal DefaultTruckRate = 5;
        private const decimal DefaultBusRate = 3.5M;
        private const decimal DefaultMotorcycleRate = 1;
        private const decimal DefaultPenaltyRate = 2.5M;
        
        public static int ParkingCapacity { get; set; }
        public static int ParkingChargingPeriod { get; set; }
        public static decimal CarRate { get; set; }
        public static decimal TruckRate { get; set; }
        public static decimal BusRate { get; set; }
        public static decimal MotorcycleRate { get; set; }
        public static decimal PenaltyRate { get; set; }

        static ParkingSettings()
        {
            ParkingCapacity = DefaultCapacity;
            ParkingChargingPeriod = DefaultChargingPeriod;
            CarRate = DefaultCarRate;
            TruckRate = DefaultTruckRate;
            BusRate = DefaultBusRate;
            MotorcycleRate = DefaultMotorcycleRate;
            PenaltyRate = DefaultPenaltyRate;
        }

        public static decimal CalculateRateByVehicleType(Vehicle vehicle)
        {
            if (vehicle.GetType() == typeof(Car))
            {
                return CarRate;
            }

            if (vehicle.GetType() == typeof(Truck))
            {
                return TruckRate;
            }

            if (vehicle.GetType() == typeof(Motorcycle))
            {
                return MotorcycleRate;
            }

            return BusRate;
        }
    }
}