namespace ParkingApp.Classes
{
    public class Truck: Vehicle
    {
        public Truck(string licenseNumber, string make = "Make not provided", string color = "Color not provided") : base(licenseNumber, make, color)
        {
            
        }
    }
}