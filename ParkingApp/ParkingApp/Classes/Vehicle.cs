using System;
using System.Collections.Generic;

namespace ParkingApp.Classes
{
    public class Vehicle
    {
        private const string ColorNotProvided = "Color not provided";
        private const string MakeNotProvided = "Make not provided";
        public string Make { get; set; }
        public string Color { get; set; }
        public string LicenseNumber { get; set; }
        
        public readonly VehicleAccountManager AccountManager = new VehicleAccountManager();
        
        private bool _isParked;

        public Vehicle(string licenseNumber, string make = MakeNotProvided, string color = ColorNotProvided )
        {
            Make = make;
            Color = color;
            LicenseNumber = licenseNumber;
        }

        public Tuple<bool, string> Park(int parkingCapacity, List<Vehicle> parkedAlready)
        {
            if (parkedAlready.Count < parkingCapacity)
            {
                parkedAlready.Add(this);
                _isParked = true;
                var outputString = string.Format("Your vehicle(" + Color + " " + Make + "," +
                                                 LicenseNumber +
                                                 ") was parked successfully. Go be pedestrian somewhere else");
                return new Tuple<bool, string>(_isParked,outputString);
            }
            else
            {
                var outputString =
                    "Sorry. Unfortunately all parking slots are occupied, try again next time";

                return new Tuple<bool, string>(_isParked, outputString);
            } 
        }

        public Tuple<bool, string> Unpark(List<Vehicle> parkedVehiclesList)
        {
            var isRemoved = parkedVehiclesList.Remove(this);

            if (isRemoved)
            {
                var outputString = string.Format("You successfully unparked your vehicle (" + Make + " " + Color + ", " +
                                                 LicenseNumber + ")");
                return new Tuple<bool, string>(true,outputString);
            }
            else
            {
                var outputString = string.Format(Make + " " + Color + "," +
                                                 LicenseNumber + " - This vehicle was not found on parking");
                return new Tuple<bool, string>(false, outputString);
            } 
        }
    }
}