using System;
using System.Collections.Generic;
using System.Threading;

namespace ParkingApp.Classes
{
    public sealed class Parking
    {
        private static readonly Parking instance = new Parking();

        public static List<Vehicle> ParkedVehicles { get; }
        public static Parking Instance => instance;

        static Parking()
        {
            ParkedVehicles = new List<Vehicle>();
            var chargingTimer = new Timer(e => ParkingAccountManager.ChargeParkedVehicles(ParkedVehicles),
                null,
                TimeSpan.Zero,
                TimeSpan.FromSeconds(ParkingSettings.ParkingChargingPeriod));
        }

        private Parking()
        {

        }

        public static string GetOccupancyStatus()
        {
            return ParkedVehicles.Count < ParkingSettings.ParkingCapacity ? 
                            string.Format($"Currently there are {ParkingSettings.ParkingCapacity - ParkedVehicles.Count} spots left") 
                            : "Sorry, parking has no free spots left";
        }
    }
}