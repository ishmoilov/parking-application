using System;

namespace ParkingApp.Classes
{
    public class Transaction
    {
        public DateTime TransactionTime { get; set; }
        public string VehicleLicenceNumber { get; set; }
        public decimal ChargedAmount { get; set; }
    }
}