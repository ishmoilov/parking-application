﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ParkingApp.Classes;

namespace ParkingAppTests
{
    [TestFixture]
    public class VehicleClassTests
    {
        readonly Vehicle _testVehicle = new Car("TEST CAR", "TEST", "TEST");
        [OneTimeSetUp]
        public void SetUp()
        {
            
        }
        [Test]
        public void ParkVehicleWhenListIsNotFull()
        {
            var testVehiclesList = new List<Vehicle> {new Bus("TEST BUS", "TEST MAKE", "TEST COLOR")};
            const int bigCapacity = 5;
            var expectedResult = string.Format("Your vehicle(TEST TEST,TEST CAR) was parked successfully. Go be pedestrian somewhere else");
            
            
            var actualResult = _testVehicle.Park(bigCapacity, testVehiclesList);
            
            
            Assert.IsTrue(actualResult.Item1);
            Assert.AreEqual(expectedResult,actualResult.Item2);
        }

        [Test]
        public void ParkVehicleWhenListIsFull()
        {
            var testVehiclesList = new List<Vehicle> {new Bus("TEST BUS", "TEST MAKE", "TEST COLOR")};
            const int smallCapacity = 1;
            const string expectedResult = "Sorry. Unfortunately all parking slots are occupied, try again next time";
            
            
            var actualResult = _testVehicle.Park(smallCapacity, testVehiclesList);
            
            
            Assert.IsFalse(actualResult.Item1);
            Assert.AreEqual(expectedResult,actualResult.Item2);
        }

        [Test]
        public void UnparkWhenCarIsParked()
        {
            var testVehiclesList = new List<Vehicle> {_testVehicle};
            var expectedString = "You successfully unparked your vehicle (TEST TEST, TEST CAR)";

            var actualResult = _testVehicle.Unpark(testVehiclesList);
            
            Assert.IsTrue(actualResult.Item1);
            Assert.AreEqual(expectedString, actualResult.Item2);
        }
        [Test]
        public void UnparkWhenCarIsNotParked()
        {
            var testVehiclesList = new List<Vehicle>();
            var expectedString = string.Format(_testVehicle.Make + " " + _testVehicle.Color + "," +
                                               _testVehicle.LicenseNumber + " - This vehicle was not found on parking");

            var actualResult = _testVehicle.Unpark(testVehiclesList);
            
            Assert.IsFalse(actualResult.Item1);
            Assert.AreEqual(expectedString,actualResult.Item2);
        }
    }
}