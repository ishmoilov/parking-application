using NUnit.Framework;
using ParkingApp.Classes;

namespace ParkingAppTests
{
    [TestFixture]
    public class VehicleAccountManagerClassTests
    {
        readonly VehicleAccountManager _testVehicleAccountManager = new VehicleAccountManager();
        [Test]
        public void VehicleAccountManagerAddFundsTest()
        {
            _testVehicleAccountManager.Balance = 10;
            var expectedBalance = 15;

            _testVehicleAccountManager.AddFunds(5);
            var actualBalanceAfterDeduction = _testVehicleAccountManager.Balance;
            
            Assert.AreEqual(expectedBalance,actualBalanceAfterDeduction);

        }

        [Test]
        public void VehicleAccountManagerWithdrawFundsWhenCanWithdraw()
        {
            _testVehicleAccountManager.Balance = 10;
            var expectedBalance = 8M;

            _testVehicleAccountManager.WithdrawFunds(2);
            var actualBalanceAfterDeduction = _testVehicleAccountManager.Balance;
            
            Assert.AreEqual(expectedBalance,actualBalanceAfterDeduction);
        }
    }
}