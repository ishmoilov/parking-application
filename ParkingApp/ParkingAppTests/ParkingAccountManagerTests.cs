using System;
using System.Collections.Generic;
using NUnit.Framework;
using ParkingApp.Classes;

namespace ParkingAppTests
{
    [TestFixture]
    public class ParkingAccountManagerTests
    {
        private static object[] _vehicleTestCases =
        {
            new Tuple<Vehicle,decimal>(new Car("TEST CAR","TEST","TEST"), ParkingSettings.CarRate),
            new Tuple<Vehicle,decimal>(new Bus("TEST BUS","TEST","TEST" ),ParkingSettings.BusRate),
            new Tuple<Vehicle,decimal>(new Truck("TEST TRUCK","TEST","TEST"),ParkingSettings.TruckRate),
            new Tuple<Vehicle,decimal>(new Motorcycle("TEST MOTO","TEST","TEST"),ParkingSettings.MotorcycleRate)
        };
        
        [Test, TestCaseSource(nameof(_vehicleTestCases))]
        public void ChargeParkedVehiclesNoPenaltyTest(Tuple<Vehicle,decimal> vehicleTestSet)
        {
            vehicleTestSet.Item1.AccountManager.Balance = 100;
            var parkedVehiclesList = new List<Vehicle>{vehicleTestSet.Item1};

            var expectedBalance = 100 - vehicleTestSet.Item2;
            
            ParkingAccountManager.ChargeParkedVehicles(parkedVehiclesList);
            var actualBalance = vehicleTestSet.Item1.AccountManager.Balance;
            
            Assert.AreEqual(expectedBalance,actualBalance);
        }

        [Test, TestCaseSource(nameof(_vehicleTestCases))]
        public void ChargeParkedVehiclesWithPenaltyTest(Tuple<Vehicle, decimal> vehicleTestSet)
        {
            vehicleTestSet.Item1.AccountManager.Balance = 0;
            var parkedVehiclesList = new List<Vehicle>{vehicleTestSet.Item1};
            var expectedBalance = 0 - (vehicleTestSet.Item2 * ParkingSettings.PenaltyRate);
            
            
            ParkingAccountManager.ChargeParkedVehicles(parkedVehiclesList);
            var actualBalance = vehicleTestSet.Item1.AccountManager.Balance;
            
            Assert.AreEqual(expectedBalance,actualBalance);
        }
    }
}